<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['404_override'] = 'Page_Not_Found';
$route['translate_uri_dashes'] = FALSE;

# Student Routing
	$route['Register_Student'] = 'Registration/add_pre_registration';

# Registrar Routing
// Registrar Landing Page
	$route['Dashboard_Registrar'] = 'Registrar';
// Pre-registered page
	$route['Pre_Registered_shs'] = 'Registrar/shs_preregistered';
// Enrolled page
	$route['Enrolled_shs'] = 'Registrar/shs_enrolled';
// Enrollment Form
	$route['Enrollment_form/(:any)'] = 'Registrar/view_registered_student/$1';
// Enroll Pre-registered student
	$route['Enroll_Student'] = 'Registrar/enroll_student';
// Registrar Registration
	$route['Add_Student_shs'] = 'Registrar/shs_add_student';
// Enroll Student by Registrar
	$route['M_enroll_this'] = 'Registrar/enroll_this_student';
// View student for update
	$route['Update_Student_shs'] = 'Registrar/view_for_update';
// Update Form
	$route['Update_SHS_Student/(:any)'] = 'Registrar/form_for_update';
// Update the student data
	$route['update_selected_student'] = 'Registrar/update_this_student';