<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration_model extends CI_Model
{
	public function register_student($data)
	{
		$query = $this->db->insert('shs_student', $data);
		return $query;
	}

	public function get_track()
	{
		$query = "SELECT * FROM track";
		$result = $this->db->query($query);
		return $result;
	}
	public function get_school_year()
	{
		$query = "SELECT * FROM school_year";
		$result = $this->db->query($query);
		return $result;
	}
	public function get_strand($track_id)
	{

		$query = $this->db->get_where('strand',array('for_track' => $track_id,));
		return $query->result();
	}
	public function get_specializations($strand_id)
	{

		$query = $this->db->get_where('specialization',array('for_strand' => $strand_id,));
		return $query->result();
	}
}