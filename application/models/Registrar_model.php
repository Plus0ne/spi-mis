<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrar_model extends CI_Model
{
	public function get_shs_student()
	{
		$query = "SELECT * FROM shs_student WHERE status = '0' ORDER BY pre_registered_date";
		$result = $this->db->query($query);
		return $result;
	}
	public function count_pre_registered()
	{
		$query = "SELECT * FROM shs_student WHERE status = '0'";
		$result = $this->db->query($query);
		return $result;
	}
	public function count_enrolled()
	{
		$query = "SELECT * FROM shs_student WHERE status = '1'";
		$result = $this->db->query($query);
		return $result;
	}
	public function get_student_data($student_no)
	{
		$query = "SELECT * FROM shs_student WHERE student_no = ?";
		$result = $this->db->query($query,$student_no);
		return $result;
	}
	public function get_track()
	{
		$query = "SELECT * FROM track";
		$result = $this->db->query($query);
		return $result;
	}
	public function get_strand($track_id)
	{
		$query = $this->db->get_where('strand',array('for_track' => $track_id,));
		return $query->result();
	}
	public function get_specializations($strand_id)
	{
		$query = $this->db->get_where('specialization',array('for_strand' => $strand_id,));
		return $query->result();
	}
	public function get_school_year()
	{
		$query = "SELECT * FROM school_year";
		$result = $this->db->query($query);
		return $result;
	}
	public function add_student($student_no,$data)
	{
		$this->db->where('student_no', $student_no);
		$this->db->update('shs_student', $data);
	}
	public function add_student_manually($data)
	{
		$this->db->insert('shs_student',$data);
	}
	public function get_enrolled_student()
	{
		$query = "SELECT * FROM shs_student WHERE status = '1' ORDER BY enrolled_date";
		$result = $this->db->query($query);
		return $result;
	}
	public function get_student_for_update()
	{
		$query = "SELECT * FROM shs_student WHERE status = '1' ORDER BY enrolled_date";
		$result = $this->db->query($query);
		return $result;
	}
	public function update_this_data($student_no,$data)
	{
		$this->db->where('student_no', $student_no);
		$this->db->update('shs_student', $data);
	}
}