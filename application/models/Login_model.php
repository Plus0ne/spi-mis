<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model
{
	public function check_student($Username,$Password)
	{
		$query = "SELECT * FROM shs_student WHERE student_id = ? AND password = ?";
		$result = $this->db->query($query,array($Username,$Password));
		return $result;
	}
	public function check_employee($Username,$Password)
	{
		$query = "SELECT * FROM employees WHERE employee_id = ? AND password = ?";
		$result = $this->db->query($query,array($Username,$Password));
		return $result;
	}
}