<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	/**
	 * [Student Controller]
	 * @return [type] [description]
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->helper('security');
	}
	public function index()
	{
		echo "Student Dashboard";
	}
}
