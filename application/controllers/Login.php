<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * [Login Controller]
	 * @return [type] [description]
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->helper('security');
	}
	public function index()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			redirect('Dashboard_Registrar');
		}
		else
		{
			$data['title'] = 'Login | Skill-Power Institute';
			$this->load->view('pages/login',$data);
		}
		
	}
	public function Login_validation()
	{
		$Username = $_POST['Username1'];
		$Password = $_POST['Password1'];
		if ($this->security->xss_clean($Username,TRUE) === FALSE || $this->security->xss_clean($Password,TRUE) === FALSE)
		{
			echo '0';
		}
		else
		{
			$check_student = $this->Login_model->check_student($Username,$Password);
			if ($check_student->num_rows() > 0)
			{
				# Student Found
				$student = $check_student->row_array();
			}
			else
			{
				$check_employee = $this->Login_model->check_employee($Username,$Password);
				if ($check_employee->num_rows() > 0)
				{
					# Check user if active
					$row = $check_employee->row_array();
					if ($row['is_active']==1) {
						echo "user active";
					}
					else
					{
						if ($row['user_type'] == 1)
						{
							$session_data = array(
							'employee_id' => $row['employee_id'],
							'password' => $row['password'],
							'is_logged_in' => 'registrar');
							// Add user data in session
							$this->session->set_userdata($session_data);
							echo "Registrar";
						}
						elseif ($row['user_type'] == 2)
						{
							echo "Marketing";
						}
						elseif ($row['user_type'] == 3) {
							echo "Accounting";
						}
						else
						{
							echo "unknown user";
						}
					}
				}
				else
				{
					echo "User dont exist";
				}
			}
		}
	}
# end of class
}
