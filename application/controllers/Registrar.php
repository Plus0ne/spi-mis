<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrar extends CI_Controller {

	/**
	 * [Registrar Controller]
	 * @return [type] [description]
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registrar_model');
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->helper('url');
	}

	// -------------------------------------------------------------------------------
	// 			This function is for loading views
	// -------------------------------------------------------------------------------
	
	public function index()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			$data['title'] = "Dashboard | Skill-Power Institute";
			$data['count_pre_registered'] = $this->Registrar_model->count_pre_registered();
			$data['count_enrolled'] = $this->Registrar_model->count_enrolled();
			$this->load->view('pages/registrar/dashboard_registrar',$data);
		}
		else
		{
			redirect('');
		}
	}
	public function shs_preregistered()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			$data['title'] = 'Pre-Registered Student | Skill-Power Institute';
			$data['get_shs_student'] = $this->Registrar_model->get_shs_student();
			$this->load->view('pages/registrar/shs_pre_registered',$data);
		}
		else
		{
			redirect('');
		}

	}
	public function shs_enrolled()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			$data['title'] = 'Enrolled Student | Skill-Power Institute';
			$data['get_enrolled_student'] = $this->Registrar_model->get_enrolled_student();
			$this->load->view('pages/registrar/shs_enrolled',$data);
		}
		else
		{
			redirect('');
		}
		
	}
	public function get_strands()
	{
		$track_id = $this->input->post('track_id');
		$strands = $this->Registrar_model->get_strand($track_id);
		if (count($strands)>0) {
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Strand </option>';
			foreach ($strands as $strand) {
				$pro_select .= '<option value="'.$strand->strand_no.'">'.$strand->acro.'</option>';
			}
			echo json_encode($pro_select);
		}
		else
		{
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Strand </option>';
			echo json_encode($pro_select);
		}
	}

	/*
		Get Specialization populate select
	 */
	public function get_specialization()
	{
		$strand_id = $this->input->post('strand_id');
		$strands = $this->Registrar_model->get_specializations($strand_id);
		if (count($strands)>0) {
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Specialization </option>';
			foreach ($specialization as $specializations) {
				$pro_select .= '<option value="'.$specializations->s_id.'">'.$specializations->specialization.'</option>';
			}
			echo json_encode($pro_select);
		}
		else
		{
			$pro_select = '';
			$pro_select .= '<script>alert("No Specialization");</script>';
			$pro_select .= '<option hidden selected>N/A</option><option>N/A</option>';
			echo json_encode($pro_select);
		}
	}

	/*
		View Pre Enrolled Student For verification
	 */
	public function view_registered_student()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			# Content Text
			$data['title'] = 'Verify | Skill-Power Institute';
			$data['Approve_Student'] = 'Approve Student';
			$data['get_track'] = $this->Registrar_model->get_track();
			$data['get_school_year'] = $this->Registrar_model->get_school_year();

			# get data id
			$student_no = $this->uri->segment(2);

			# sql query select student base on id
			$get_student_data = $this->Registrar_model->get_student_data($student_no);
			$row = $get_student_data->row();

			# variables student data
			$data['student_no'] = $row->student_no;
			$data['student_id'] = $row->student_id;
			$data['password'] = $row->password;
			$data['lrn'] = $row->lrn;
			$data['voucher'] = $row->voucher;

			if ($row->student_status == 1) {
				$data['active1'] = 'checked=""';
				$data['active2'] = '';
				$data['active3'] = '';
			}
			elseif ($row->student_status == 2) {
				$data['active1'] = '';
				$data['active2'] = 'checked=""';
				$data['active3'] = '';
			}
			elseif ($row->student_status == 3) {
				$data['active1'] = '';
				$data['active2'] = '';
				$data['active3'] = 'checked=""';
			}
			else
			{
				echo "error";
			}
			$data['last_name'] = $row->last_name;
			$data['first_name'] = $row->first_name;
			$data['middle_name'] = $row->middle_name;
			$data['name_ext'] = $row->name_ext;
			$data['current_address'] = $row->current_address;
			$data['current_barangay'] = $row->current_barangay;
			$data['current_municipality'] = $row->current_municipality;
			$data['current_province'] = $row->current_province;
			$data['date_of_birth'] = $row->date_of_birth;
			$data['student_age'] = $row->student_age;
			$data['bp_address'] = $row->bp_address;
			$data['bp_barangay'] = $row->bp_barangay;
			$data['bp_municipality'] = $row->bp_municipality;
			$data['bp_province'] = $row->bp_province;
			$data['religion'] = $row->religion;
			$data['civil_status'] = $row->civil_status;
			$data['student_gender'] = $row->student_gender;
			$data['facebook_account'] = $row->facebook_account;
			$data['mobile_no'] = $row->mobile_no;
			$data['tel_no'] = $row->tel_no;
			$data['email_address'] = $row->email_address;
			$data['ethnicity'] = $row->ethnicity;
			$data['mother_tongue'] = $row->mother_tongue;
			$data['m_last_name'] = $row->m_last_name;
			$data['m_first_name'] = $row->m_first_name;
			$data['m_middle_name'] = $row->m_middle_name;
			$data['m_name_ext'] = $row->m_name_ext;
			$data['m_contact'] = $row->m_contact;
			$data['f_last_name'] = $row->f_last_name;
			$data['f_first_name'] = $row->f_first_name;
			$data['f_middle_name'] = $row->f_middle_name;
			$data['f_name_ext'] = $row->f_name_ext;
			$data['f_contact'] = $row->f_contact;
			$data['pg_last_name'] = $row->pg_last_name;
			$data['pg_first_name'] = $row->pg_first_name;
			$data['pg_middle_name'] = $row->pg_middle_name;
			$data['pg_name_ext'] = $row->pg_name_ext;
			$data['pg_current_address'] = $row->pg_current_address;
			$data['pg_relation'] = $row->pg_relation;
			$data['pg_tel_no'] = $row->pg_tel_no;
			$data['pg_zip_code'] = $row->pg_zip_code;
			$data['nops'] = $row->nops;
			$data['nops_grade_level'] = $row->nops_grade_level;
			$data['nops_section'] = $row->nops_section;
			$data['nops_school_type'] = $row->nops_school_type;
			$data['track'] = $row->track;
			$data['strand'] = $row->strand;
			$data['specialization'] = $row->specialization;
			$data['grade'] = $row->grade;
			$data['school_year'] = $row->school_year;
			$data['semester'] = $row->semester;
			$data['pre_registered_date'] = $row->pre_registered_date;


			# load view with data
			$this->load->view('pages/registrar/shs_enrollment_form',$data);
		}
		else
		{
			redirect('');
		}
		
	}

	// -------------------------------------------------------------------------------
	// 			This function is for verification of pre-registered student
	// -------------------------------------------------------------------------------
	
	public function enroll_student()
	{
		/* Prepare varibles */
		date_default_timezone_set('Asia/Manila');
		$enrolled_date = date('Y/m/d - h:i a');
		$student_no = $this->input->post('student_no');
		$status = '1';
		$data = array
		(
			'student_id' => $this->input->post('student_id'),
			'password' => $this->input->post('password'),
			'lrn' => $this->input->post('lrn'),
			'voucher' => $this->input->post('voucher'),
			'student_status' => $this->input->post('radio-stacked'),
			'last_name' => $this->input->post('last_name'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'name_ext' => $this->input->post('name_ext'),
			'current_address' => $this->input->post('current_address'),
			'current_barangay' => $this->input->post('current_barangay'),
			'current_municipality' => $this->input->post('current_municipality'),
			'current_province' => $this->input->post('current_province'),
			'date_of_birth' => $this->input->post('date_of_birth'),
			'student_age' => $this->input->post('student_age'),
			'bp_address' => $this->input->post('bp_address'),
			'bp_barangay' => $this->input->post('bp_barangay'),
			'bp_municipality' => $this->input->post('bp_municipality'),
			'bp_province' => $this->input->post('bp_province'),
			'religion' => $this->input->post('religion'),
			'civil_status' => $this->input->post('civil_status'),
			'student_gender' => $this->input->post('student_gender'),
			'facebook_account' => $this->input->post('facebook_account'),
			'mobile_no' => $this->input->post('mobile_no'),
			'tel_no' => $this->input->post('tel_no'),
			'email_address' => $this->input->post('email_address'),
			'ethnicity' => $this->input->post('ethnicity'),
			'mother_tongue' => $this->input->post('mother_tongue'),
			'm_last_name' => $this->input->post('m_last_name'),
			'm_first_name' => $this->input->post('m_first_name'),
			'm_middle_name' => $this->input->post('m_middle_name'),
			'm_name_ext' => $this->input->post('m_name_ext'),
			'm_contact' => $this->input->post('m_contact'),
			'f_last_name' => $this->input->post('f_last_name'),
			'f_first_name' => $this->input->post('f_first_name'),
			'f_middle_name' => $this->input->post('f_middle_name'),
			'f_name_ext' => $this->input->post('f_name_ext'),
			'f_contact' => $this->input->post('f_contact'),
			'pg_last_name' => $this->input->post('pg_last_name'),
			'pg_first_name' => $this->input->post('pg_first_name'),
			'pg_middle_name' => $this->input->post('pg_middle_name'),
			'pg_name_ext' => $this->input->post('pg_name_ext'),
			'pg_current_address' => $this->input->post('pg_current_address'),
			'pg_relation' => $this->input->post('pg_relation'),
			'pg_tel_no' => $this->input->post('pg_tel_no'),
			'pg_zip_code' => $this->input->post('pg_zip_code'),
			'nops' => $this->input->post('nops'),
			'nops_grade_level' => $this->input->post('nops_grade_level'),
			'nops_section' => $this->input->post('nops_section'),
			'nops_school_type' => $this->input->post('nops_school_type'),
			'track' => $this->input->post('track'),
			'strand' => $this->input->post('strand'),
			'specialization' => $this->input->post('specialization'),
			'school_year' => $this->input->post('school_year'),
			'grade' => $this->input->post('grade'),
			'semester' => $this->input->post('semester'),
			'password' => $this->input->post('password'),
			'pre_registered_date' => $this->input->post('pre_registered_date'),
			'enrolled_date' => $enrolled_date,
			'birth_certificate' => $this->input->post('birth_certificate'),
			'two_by_two_picture' => $this->input->post('two_by_two_picture'),
			'form_138' => $this->input->post('form_138'),
			'good_moral' => $this->input->post('good_moral'),
			'status' => $status,
		);
		if ($this->security->xss_clean($student_no) == true) 
		{
			if ($this->security->xss_clean($data) == true)
			{
				$add_student = $this->Registrar_model->add_student($student_no,$data);
				$this->session->set_flashdata('succes_alert','<script>alert("SUCCESS");</scipt>');
				redirect('Pre_Registered_shs');
			}
		}
		else
		{
			echo "error";
		}
		
	}
	public function shs_add_student()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			/* Content */
			$data['title'] = "Add SHS Student | Skill-Power Institute";
			$data['add_student'] = 'Add Student';
			$data['get_track'] = $this->Registrar_model->get_track();
			$data['get_school_year'] = $this->Registrar_model->get_school_year();

			$this->load->view('pages/registrar/shs_add_student',$data);
		}
		else
		{
			redirect('');
		}
	}
	public function enroll_this_student()
	{
		/* Prepare varibles */
		date_default_timezone_set('Asia/Manila');
		$pre_registered_date = date('Y/m/d - h:i a');
		$enrolled_date = date('Y/m/d - h:i a');

		if ($this->input->post('birth_certificate') == '') {
			$birth_certificate = 0;
		}
		else
		{
			$birth_certificate = $this->input->post('birth_certificate');
		}

		if ($this->input->post('two_by_two_picture') == '') {
			$two_by_two_picture = 0;
		}
		else
		{
			$two_by_two_picture = $this->input->post('two_by_two_picture');
		}
		if ($this->input->post('form_138') == '') {
			$form_138 = 0;
		}
		else
		{
			$form_138 = $this->input->post('form_138');
		}
		if ($this->input->post('good_moral') == '') {
			$good_moral = 0;
		}
		else
		{
			$good_moral = $this->input->post('good_moral');
		}
		$student_no = $this->input->post('student_no');
		$status = '1';

		$data = array
		(
			'student_id' => $this->input->post('student_id'),
			'password' => $this->input->post('password'),
			'lrn' => $this->input->post('lrn'),
			'voucher' => $this->input->post('voucher'),
			'student_status' => $this->input->post('radio-stacked'),
			'last_name' => $this->input->post('last_name'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'name_ext' => $this->input->post('name_ext'),
			'current_address' => $this->input->post('current_address'),
			'current_barangay' => $this->input->post('current_barangay'),
			'current_municipality' => $this->input->post('current_municipality'),
			'current_province' => $this->input->post('current_province'),
			'date_of_birth' => $this->input->post('date_of_birth'),
			'student_age' => $this->input->post('student_age'),
			'bp_address' => $this->input->post('bp_address'),
			'bp_barangay' => $this->input->post('bp_barangay'),
			'bp_municipality' => $this->input->post('bp_municipality'),
			'bp_province' => $this->input->post('bp_province'),
			'religion' => $this->input->post('religion'),
			'civil_status' => $this->input->post('civil_status'),
			'student_gender' => $this->input->post('student_gender'),
			'facebook_account' => $this->input->post('facebook_account'),
			'mobile_no' => $this->input->post('mobile_no'),
			'tel_no' => $this->input->post('tel_no'),
			'email_address' => $this->input->post('email_address'),
			'ethnicity' => $this->input->post('ethnicity'),
			'mother_tongue' => $this->input->post('mother_tongue'),
			'm_last_name' => $this->input->post('m_last_name'),
			'm_first_name' => $this->input->post('m_first_name'),
			'm_middle_name' => $this->input->post('m_middle_name'),
			'm_name_ext' => $this->input->post('m_name_ext'),
			'm_contact' => $this->input->post('m_contact'),
			'f_last_name' => $this->input->post('f_last_name'),
			'f_first_name' => $this->input->post('f_first_name'),
			'f_middle_name' => $this->input->post('f_middle_name'),
			'f_name_ext' => $this->input->post('f_name_ext'),
			'f_contact' => $this->input->post('f_contact'),
			'pg_last_name' => $this->input->post('pg_last_name'),
			'pg_first_name' => $this->input->post('pg_first_name'),
			'pg_middle_name' => $this->input->post('pg_middle_name'),
			'pg_name_ext' => $this->input->post('pg_name_ext'),
			'pg_current_address' => $this->input->post('pg_current_address'),
			'pg_relation' => $this->input->post('pg_relation'),
			'pg_tel_no' => $this->input->post('pg_tel_no'),
			'pg_zip_code' => $this->input->post('pg_zip_code'),
			'nops' => $this->input->post('nops'),
			'nops_grade_level' => $this->input->post('nops_grade_level'),
			'nops_section' => $this->input->post('nops_section'),
			'nops_school_type' => $this->input->post('nops_school_type'),
			'track' => $this->input->post('track'),
			'strand' => $this->input->post('strand'),
			'specialization' => $this->input->post('specialization'),
			'school_year' => $this->input->post('school_year'),
			'grade' => $this->input->post('grade'),
			'semester' => $this->input->post('semester'),
			'pre_registered_date' =>$pre_registered_date,
			'enrolled_date' => $enrolled_date,
			'birth_certificate' => $birth_certificate,
			'two_by_two_picture' => $two_by_two_picture,
			'form_138' => $form_138,
			'good_moral' => $good_moral,
			'status' => $status,
		);

		$add_student_manually = $this->Registrar_model->add_student_manually($data);

		$this->session->set_flashdata('alert','<div class="alert alert-success" role="alert">
                                <h4 class="alert-heading">Student Enrolled!</h4>
                            </div>');
		redirect('Add_Student_shs');
		
	}

	// -------------------------------------------------------------------------------
	// 			This function is for updating student
	// -------------------------------------------------------------------------------
	
	public function view_for_update()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			$data['title'] = 'Update Student | Skill-Power Institute';
			$data['get_student_for_update'] = $this->Registrar_model->get_student_for_update();
			$this->load->view('pages/registrar/shs_view_student_up',$data);
		}
		else
		{
			redirect('');
		}
	}
	public function form_for_update()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			# Content Text
			$data['title'] = 'Update | Skill-Power Institute';
			$data['Approve_Student'] = 'Update Student';
			$data['get_track'] = $this->Registrar_model->get_track();
			$data['get_school_year'] = $this->Registrar_model->get_school_year();

			# get data id
			$student_no = $this->uri->segment(2);

			# sql query select student base on id
			$get_student_data = $this->Registrar_model->get_student_data($student_no);
			$row = $get_student_data->row();

			# variables student data
			$data['student_no'] = $row->student_no;
			$data['student_id'] = $row->student_id;
			$data['password'] = $row->password;
			$data['lrn'] = $row->lrn;
			$data['voucher'] = $row->voucher;

			if ($row->student_status == 1) {
				$data['active1'] = 'checked=""';
				$data['active2'] = '';
				$data['active3'] = '';
			}
			elseif ($row->student_status == 2) {
				$data['active1'] = '';
				$data['active2'] = 'checked=""';
				$data['active3'] = '';
			}
			elseif ($row->student_status == 3) {
				$data['active1'] = '';
				$data['active2'] = '';
				$data['active3'] = 'checked=""';
			}
			else
			{
				echo "error";
			}
			$data['last_name'] = $row->last_name;
			$data['first_name'] = $row->first_name;
			$data['middle_name'] = $row->middle_name;
			$data['name_ext'] = $row->name_ext;
			$data['current_address'] = $row->current_address;
			$data['current_barangay'] = $row->current_barangay;
			$data['current_municipality'] = $row->current_municipality;
			$data['current_province'] = $row->current_province;
			$data['date_of_birth'] = $row->date_of_birth;
			$data['student_age'] = $row->student_age;
			$data['bp_address'] = $row->bp_address;
			$data['bp_barangay'] = $row->bp_barangay;
			$data['bp_municipality'] = $row->bp_municipality;
			$data['bp_province'] = $row->bp_province;
			$data['religion'] = $row->religion;
			$data['civil_status'] = $row->civil_status;
			$data['student_gender'] = $row->student_gender;
			$data['facebook_account'] = $row->facebook_account;
			$data['mobile_no'] = $row->mobile_no;
			$data['tel_no'] = $row->tel_no;
			$data['email_address'] = $row->email_address;
			$data['ethnicity'] = $row->ethnicity;
			$data['mother_tongue'] = $row->mother_tongue;
			$data['m_last_name'] = $row->m_last_name;
			$data['m_first_name'] = $row->m_first_name;
			$data['m_middle_name'] = $row->m_middle_name;
			$data['m_name_ext'] = $row->m_name_ext;
			$data['m_contact'] = $row->m_contact;
			$data['f_last_name'] = $row->f_last_name;
			$data['f_first_name'] = $row->f_first_name;
			$data['f_middle_name'] = $row->f_middle_name;
			$data['f_name_ext'] = $row->f_name_ext;
			$data['f_contact'] = $row->f_contact;
			$data['pg_last_name'] = $row->pg_last_name;
			$data['pg_first_name'] = $row->pg_first_name;
			$data['pg_middle_name'] = $row->pg_middle_name;
			$data['pg_name_ext'] = $row->pg_name_ext;
			$data['pg_current_address'] = $row->pg_current_address;
			$data['pg_relation'] = $row->pg_relation;
			$data['pg_tel_no'] = $row->pg_tel_no;
			$data['pg_zip_code'] = $row->pg_zip_code;
			$data['nops'] = $row->nops;
			$data['nops_grade_level'] = $row->nops_grade_level;
			$data['nops_section'] = $row->nops_section;
			$data['nops_school_type'] = $row->nops_school_type;
			$data['track'] = $row->track;
			$data['strand'] = $row->strand;
			$data['specialization'] = $row->specialization;
			$data['grade'] = $row->grade;
			$data['school_year'] = $row->school_year;
			$data['semester'] = $row->semester;
			$data['pre_registered_date'] = $row->pre_registered_date;
			$data['enrolled_date'] = $row->enrolled_date;
			if ($row->birth_certificate == 1) {
				$data['birth_certificate'] = 'checked=""';
			}
			else
			{
				$data['birth_certificate'] = '';
			}
			if ($row->two_by_two_picture == 1) {
				$data['two_by_two_picture'] = 'checked=""';
			}
			else
			{
				$data['two_by_two_picture'] = '';
			}
			if ($row->form_138 == 1) {
				$data['form_138'] = 'checked=""';
			}
			else
			{
				$data['form_138'] = '';
			}
			if ($row->good_moral == 1) {
				$data['good_moral'] = 'checked=""';
			}
			else
			{
				$data['good_moral'] = '';
			}

			$this->load->view('pages/registrar/shs_form_for_update',$data);
		}
		else
		{
			redirect('');
		}
	}
	public function update_this_student()
	{
		if ($this->session->userdata('is_logged_in') == 'registrar')
		{
			/* Prepare variables */
			if ($this->input->post('birth_certificate') == '') {
				$birth_certificate = 0;
			}
			else
			{
				$birth_certificate = $this->input->post('birth_certificate');
			}

			if ($this->input->post('two_by_two_picture') == '') {
				$two_by_two_picture = 0;
			}
			else
			{
				$two_by_two_picture = $this->input->post('two_by_two_picture');
			}
			if ($this->input->post('form_138') == '') {
				$form_138 = 0;
			}
			else
			{
				$form_138 = $this->input->post('form_138');
			}
			if ($this->input->post('good_moral') == '') {
				$good_moral = 0;
			}
			else
			{
				$good_moral = $this->input->post('good_moral');
			}

			$student_no = $this->input->post('student_no');
			$status = '1';

			$data = array
			(
				'student_id' => $this->input->post('student_id'),
				'password' => $this->input->post('password'),
				'lrn' => $this->input->post('lrn'),
				'voucher' => $this->input->post('voucher'),
				'student_status' => $this->input->post('radio-stacked'),
				'last_name' => $this->input->post('last_name'),
				'first_name' => $this->input->post('first_name'),
				'middle_name' => $this->input->post('middle_name'),
				'name_ext' => $this->input->post('name_ext'),
				'current_address' => $this->input->post('current_address'),
				'current_barangay' => $this->input->post('current_barangay'),
				'current_municipality' => $this->input->post('current_municipality'),
				'current_province' => $this->input->post('current_province'),
				'date_of_birth' => $this->input->post('date_of_birth'),
				'student_age' => $this->input->post('student_age'),
				'bp_address' => $this->input->post('bp_address'),
				'bp_barangay' => $this->input->post('bp_barangay'),
				'bp_municipality' => $this->input->post('bp_municipality'),
				'bp_province' => $this->input->post('bp_province'),
				'religion' => $this->input->post('religion'),
				'civil_status' => $this->input->post('civil_status'),
				'student_gender' => $this->input->post('student_gender'),
				'facebook_account' => $this->input->post('facebook_account'),
				'mobile_no' => $this->input->post('mobile_no'),
				'tel_no' => $this->input->post('tel_no'),
				'email_address' => $this->input->post('email_address'),
				'ethnicity' => $this->input->post('ethnicity'),
				'mother_tongue' => $this->input->post('mother_tongue'),
				'm_last_name' => $this->input->post('m_last_name'),
				'm_first_name' => $this->input->post('m_first_name'),
				'm_middle_name' => $this->input->post('m_middle_name'),
				'm_name_ext' => $this->input->post('m_name_ext'),
				'm_contact' => $this->input->post('m_contact'),
				'f_last_name' => $this->input->post('f_last_name'),
				'f_first_name' => $this->input->post('f_first_name'),
				'f_middle_name' => $this->input->post('f_middle_name'),
				'f_name_ext' => $this->input->post('f_name_ext'),
				'f_contact' => $this->input->post('f_contact'),
				'pg_last_name' => $this->input->post('pg_last_name'),
				'pg_first_name' => $this->input->post('pg_first_name'),
				'pg_middle_name' => $this->input->post('pg_middle_name'),
				'pg_name_ext' => $this->input->post('pg_name_ext'),
				'pg_current_address' => $this->input->post('pg_current_address'),
				'pg_relation' => $this->input->post('pg_relation'),
				'pg_tel_no' => $this->input->post('pg_tel_no'),
				'pg_zip_code' => $this->input->post('pg_zip_code'),
				'nops' => $this->input->post('nops'),
				'nops_grade_level' => $this->input->post('nops_grade_level'),
				'nops_section' => $this->input->post('nops_section'),
				'nops_school_type' => $this->input->post('nops_school_type'),
				'track' => $this->input->post('track'),
				'strand' => $this->input->post('strand'),
				'specialization' => $this->input->post('specialization'),
				'school_year' => $this->input->post('school_year'),
				'grade' => $this->input->post('grade'),
				'semester' => $this->input->post('semester'),
				'pre_registered_date' => $this->input->post('pre_registered_date'),
				'enrolled_date' => $this->input->post('enrolled_date'),
				'birth_certificate' => $birth_certificate,
				'two_by_two_picture' => $two_by_two_picture,
				'form_138' => $form_138,
				'good_moral' => $good_moral,
				'status' => $status,
			);

			$updated = $this->Registrar_model->update_this_data($student_no,$data);
			
			$this->session->set_flashdata('alert','<div class="alert alert-success" role="alert">
                                <h4 class="alert-heading">Student updated!</h4>
                            </div>');
			redirect('Update_Student_shs');
		}
		else
		{
			redirect('');
		}
	}

	// end here
}
