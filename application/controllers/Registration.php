<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registration extends CI_Controller {

	/**
	 * [Registration Controller]
	 * @return [type] [description]
	 */
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Registration_model');
		$this->load->helper('security');
		$this->load->helper('form');
		$this->load->library('session');
	}
	public function index()
	{
		$data['title'] = "Pre-registration | Skill-Power Institute";
		$data['tracks'] = $this->Registration_model->get_track();
		$data['school_year'] = $this->Registration_model->get_school_year();
		$this->load->view('pages/registration_form',$data);
	}
	public function add_pre_registration()
	{
		# Preparations
		if ($this->input->post('radiobtns') == 1 || $this->input->post('radiobtns') == 3)
		{
			$student_id = "n/a";
		}
		else
		{
			$student_id = $this->input->post('student_id');
		}
		$lrn = 'N/A';
		$voucher = 'N/A';

		date_default_timezone_set('Asia/Manila');
		$pre_registered_date = date('Y/m/d - h:i a');
		$password = 'N/A';

		# Variable Array
		$data = array
		(
			'student_id' => $student_id,
			'lrn' => $lrn,
			'voucher' => $voucher,
			'student_status' => $this->input->post('radiobtns'),
			'last_name' => $this->input->post('last_name'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'name_ext' => $this->input->post('name_ext'),
			'current_address' => $this->input->post('current_address'),
			'current_barangay' => $this->input->post('current_barangay'),
			'current_municipality' => $this->input->post('current_municipality'),
			'current_province' => $this->input->post('current_province'),
			'date_of_birth' => $this->input->post('date_of_birth'),
			'student_age' => $this->input->post('student_age'),
			'bp_address' => $this->input->post('bp_address'),
			'bp_barangay' => $this->input->post('bp_barangay'),
			'bp_municipality' => $this->input->post('bp_municipality'),
			'bp_province' => $this->input->post('bp_province'),
			'religion' => $this->input->post('religion'),
			'civil_status' => $this->input->post('civil_status'),
			'student_gender' => $this->input->post('student_gender'),
			'facebook_account' => $this->input->post('facebook_account'),
			'mobile_no' => $this->input->post('mobile_no'),
			'tel_no' => $this->input->post('tel_no'),
			'email_address' => $this->input->post('email_address'),
			'ethnicity' => $this->input->post('ethnicity'),
			'mother_tongue' => $this->input->post('mother_tongue'),
			'm_last_name' => $this->input->post('m_last_name'),
			'm_first_name' => $this->input->post('m_first_name'),
			'm_middle_name' => $this->input->post('m_middle_name'),
			'm_name_ext' => $this->input->post('m_name_ext'),
			'm_contact' => $this->input->post('m_contact'),
			'f_last_name' => $this->input->post('f_last_name'),
			'f_first_name' => $this->input->post('f_first_name'),
			'f_middle_name' => $this->input->post('f_middle_name'),
			'f_name_ext' => $this->input->post('f_name_ext'),
			'f_contact' => $this->input->post('f_contact'),
			'pg_last_name' => $this->input->post('pg_last_name'),
			'pg_first_name' => $this->input->post('pg_first_name'),
			'pg_middle_name' => $this->input->post('pg_middle_name'),
			'pg_name_ext' => $this->input->post('pg_name_ext'),
			'pg_current_address' => $this->input->post('pg_current_address'),
			'pg_relation' => $this->input->post('pg_relation'),
			'pg_tel_no' => $this->input->post('pg_tel_no'),
			'pg_zip_code' => $this->input->post('pg_zip_code'),
			'nops' => $this->input->post('nops'),
			'nops_grade_level' => $this->input->post('nops_grade_level'),
			'nops_section' => $this->input->post('nops_section'),
			'nops_school_type' => $this->input->post('nops_school_type'),
			'track' => $this->input->post('track'),
			'strand' => $this->input->post('strand'),
			'specialization' => $this->input->post('specialization'),
			'school_year' => $this->input->post('school_year'),
			'grade' => $this->input->post('grade'),
			'semester' => $this->input->post('semester'),
			'password' => $password,
			'pre_registered_date' => $pre_registered_date,
		);

		$register = $this->Registration_model->register_student($data);
		if ($register == true) {
			redirect('');
		}
		else
		{
			redirect('Registration');
		}

	}
	public function get_strands()
	{
		$track_id = $this->input->post('track_id');
		$strands = $this->Registration_model->get_strand($track_id);
		if (count($strands)>0) {
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Strand </option>';
			foreach ($strands as $strand) {
				$pro_select .= '<option value="'.$strand->acro.'">'.$strand->acro.'</option>';
			}
			echo json_encode($pro_select);
		}
		else
		{
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Strand </option>';
			echo json_encode($pro_select);
		}
	}
	public function get_specialization()
	{
		$strand_id = $this->input->post('strand_id');
		$strands = $this->Registration_model->get_specializations($strand_id);
		if (count($strands)>0) {
			$pro_select = '';
			$pro_select .= '<option hidden disabled selected> Specialization </option>';
			foreach ($specialization as $specializations) {
				$pro_select .= '<option value="'.$specializations->specialization.'">'.$specializations->specialization.'</option>';
			}
			echo json_encode($pro_select);
		}
		else
		{
			$pro_select = '';
			$pro_select .= '<script>alert("No Specialization");</script>';
			$pro_select .= '<option hidden selected value="N/A"> Specialization </option>';
			echo json_encode($pro_select);
		}
	}
	
}
