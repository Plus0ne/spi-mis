<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	/**
	 * [Logout Controller]
	 * @return [type] [description]
	 */
	
	public function index()
	{
		/*
		Start
		*/
		$this->session->sess_destroy();
		redirect('');
	}
}
