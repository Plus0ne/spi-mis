<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_Not_Found extends CI_Controller {
	
	public function index()
	{
		$data['title'] = 'Page Not Found | Skill-Power Institute';
		$this->load->view('pages/page404',$data);
	}
}
