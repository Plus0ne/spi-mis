        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?=base_url()?>Dashboard_Registrar" aria-expanded="false">
                                <i class="mdi mdi-view-dashboard"></i><span class="hide-menu"> Dashboard </span>
                            </a>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu"> Pre-Registered </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> College </span></a></li>
                                <li class="sidebar-item"><a href="<?=base_url()?>Pre_Registered_shs" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> SHS </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-border-outside"></i><span class="hide-menu"> Enrolled </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> College </span></a></li>
                                <li class="sidebar-item"><a href="<?=base_url()?>Enrolled_shs" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> SHS </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-list"></i><span class="hide-menu"> Master List </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> College </span></a></li>
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-view-list"></i><span class="hide-menu"> SHS </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false">
                                <i class="mdi mdi-arrow-all"></i><span class="hide-menu"> Sections </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false">
                                <i class="mdi mdi-receipt"></i><span class="hide-menu"> Grades </span>
                            </a>
                        </li>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="#" aria-expanded="false"><i class="mdi mdi-relative-scale"></i><span class="hide-menu"> Schedule </span>
                            </a>
                        </li>

                        <div style="float: left;width: 100%; border-bottom: 1px solid #3C3B3B;"></div>

                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu"> Senior High </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?=base_url()?>Add_Student_shs" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Add Student </span></a></li>
                                <li class="sidebar-item"><a href="<?=base_url()?>Update_Student_shs" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Update Student </span></a></li>
                                <li class="sidebar-item"><a data-toggle="modal" data-target="#add_track" href="#" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Add Track </span></a></li>
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Add Strand </span></a></li>
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Add Specialization </span></a></li>
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-plus"></i><span class="hide-menu"> Add Subject </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-settings"></i><span class="hide-menu"> College </span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-account-plus"></i><span class="hide-menu"> Senior High </span></a></li>
                                <li class="sidebar-item"><a href="#" class="sidebar-link"><i class="mdi mdi-account-plus"></i><span class="hide-menu"> College </span></a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>