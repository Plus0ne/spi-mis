<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
        <title>
            <?php echo $title;?>
        </title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>assets/custom/img/spi-logo-glow-small.png">
    <link href="<?=base_url()?>assets/matrix_admin/libs/flot/css/float-chart.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/matrix_admin/extra-libs/multicheck/multicheck.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/matrix_admin/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css">
    <link href="<?=base_url()?>assets/matrix_admin/libs/toastr/build/toastr.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/matrix_admin/dist/css/style.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/matrix_admin/libs/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/matrix_admin/libs/jquery-minicolors/jquery.minicolors.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/matrix_admin/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>assets/matrix_admin/libs/quill/dist/quill.snow.css">
</head>