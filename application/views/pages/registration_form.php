<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title> <?php echo $title;?> </title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?=base_url()?>assets/egrappler/css/bootstrap.min.css" rel="stylesheet">
<link href="<?=base_url()?>assets/egrappler/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
<link href="<?=base_url()?>assets/egrappler/css/font-awesome.css" rel="stylesheet">
<link href="<?=base_url()?>assets/egrappler/css/style.css" rel="stylesheet">
<link href="<?=base_url()?>assets/egrappler/css/pages/dashboard.css" rel="stylesheet">
</head>
<body>
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?=base_url()?>">
          <img src="<?=base_url()?>assets/custom/img/spi logo with inc.png" alt="spi logo with inc" width="70px"> SHS Student Pre-registration </a>
    </div>
  </div>
</div>
<div class="main">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <div class="span12">
            <div id="message">

            </div>

          <form action="<?=base_url()?>Register_Student" method="POST">
          <div class="widget-content" style="margin-top: 10px; margin-bottom: 10px;">
            <div class="control-group">
              <label class="control-label">
                <h4>
                  <i>College Student? <a href="#">Click here!</a></i>
                </h4>
              </label>
              <br>
              <div id="in_stud"></div>
              <div class="controls">
                <label class="radio inline">
                  <input checked="" onclick="javascript:StudID_Show();" name="radiobtns" type="radio" value="1"> New
                </label>
                <label class="radio inline">
                  <input onclick="javascript:StudID_Show();" id="Old_Select" name="radiobtns" type="radio" value="2"> Old
                </label>
                <label class="radio inline">
                  <input onclick="javascript:StudID_Show();" name="radiobtns" type="radio" value="3"> Transferee
                </label>
                <label class="radio inline">

                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="span12">
          <div class="widget-content" style="margin-top: 10px; margin-bottom: 10px;">
              <h3> Personal Information </h3>
              <div style="border-top: 1px solid #DCDCDC; float: left; width: 100%; margin-top: 20px; margin-bottom: 20px;"></div>
              <div class="control-group">
                <label class="control-label">
                  Complete Name
                </label>
                <div class="controls">
                  <input id="LastName" name="last_name" class="span2" value="" type="text" placeholder="Last Name" required="">
                  <input class="span2" name="first_name" value="" type="text" placeholder="First Name" required="">
                  <input id="MiddleName" name="middle_name" class="span2" value="" type="text" placeholder="Middle Name" required="">
                  <input class="span1" name="name_ext" value="" type="text" placeholder="Ext.">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                  Address
                </label>
                <div class="controls">
                  <input class="span3" name="current_address" value="" type="text" placeholder="Block/Lot/Street">
                  <input class="span2" name="current_barangay" value="" type="text" placeholder="Barangay">
                  <input class="span2" name="current_municipality" value="" type="text" placeholder="Municipality">
                  <input class="span2" name="current_province" value="" type="text" placeholder="Province">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                  Date Of Birth
                </label>
                <div class="controls">
                  <input class="span2" name="date_of_birth" value="" type="date" placeholder="Month">
                  <input class="span1" name="student_age" value="" type="text" placeholder="Age">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                  Birth Place
                </label>
                <div class="controls">
                  <input class="span3" name="bp_address" value="" type="text" placeholder="Block/Lot/Street">
                  <input class="span2" name="bp_barangay" value="" type="text" placeholder="Barangay">
                  <input class="span2" name="bp_municipality" value="" type="text" placeholder="Municipality">
                  <input class="span2" name="bp_province" value="" type="text" placeholder="Province">
                </div>
              </div>
              <div style="border-top: 1px solid #DCDCDC; float: left; width: 100%; margin-top: 20px; margin-bottom: 20px;"></div>
              <div class="control-group">
                <div class="controls">
                  <input name="religion" class="span2" value="" type="text" placeholder="Religion">
                  <select name="civil_status" class="selectpicker" style="width: 100px;" required="">
                    <option selected="" disabled="" hidden="" value="">Civil Status</option>
                    <option>Single</option>
                    <option>Married</option>
                    <option>Widowed</option>
                  </select>
                  <select name="student_gender" class="selectpicker" style="width: 100px;" required="">
                    <option selected="" disabled="" hidden="" value="">Gender</option>
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                  <input name="facebook_account" class="span3" value="" type="text" placeholder="Facebook Account">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <input class="span2" name="mobile_no" value="" type="text" placeholder="Mobile Number">
                  <input class="span2" name="tel_no" value="" type="text" placeholder="Tel. Number">
                  <input class="span3" name="email_address" value="" type="email" placeholder="Email Address">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <input class="span2" name="ethnicity" value="" type="text" placeholder="Ethnicity">
                  <input class="span2" name="mother_tongue" value="" type="text" placeholder="Mother Tongue">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                    Mother's information
                </label>
                <div class="controls">
                    <input class="span2" name="m_last_name" value="" type="text" placeholder="Last Name">
                    <input class="span2" name="m_first_name" value="" type="text" placeholder="First Name">
                    <input class="span2" name="m_middle_name" value="" type="text" placeholder="Middle Name">
                    <input class="span1" name="m_name_ext" value="" type="text" placeholder="Ext.">
                    <input class="span2" name="m_contact" value="" type="text" placeholder="Contact Number">
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">
                    Father's information
                </label>
                <div class="controls">
                    <input class="span2" name="f_last_name" value="" type="text" placeholder="Last Name">
                    <input class="span2" name="f_first_name" value="" type="text" placeholder="First Name">
                    <input class="span2" name="f_middle_name" value="" type="text" placeholder="Middle Name">
                    <input class="span1" name="f_name_ext" value="" type="text" placeholder="Ext.">
                    <input class="span2" name="f_contact" value="" type="text" placeholder="Contact Number">
                </div>
              </div>
          </div>
          <div class="widget-content">
              <h3> In case of emergency, Please contact: </h3>
              <div style="border-top: 1px solid #DCDCDC; float: left; width: 100%; margin-top: 20px; margin-bottom: 20px;"></div>
              <div class="control-group">
                <label class="control-label">
                    Parent/Guardian
                </label>
                <div class="controls">
                    <input class="span2" name="pg_last_name" value="" type="text" placeholder="Last Name">
                    <input class="span2" name="pg_first_name" value="" type="text" placeholder="First Name">
                    <input class="span2" name="pg_middle_name" value="" type="text" placeholder="Middle Name">
                    <input class="span1" name="pg_name_ext" value="" type="text" placeholder="Ext.">

                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <input name="pg_current_address" class="span5" value="" type="text" placeholder="Address">
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                    <input class="span2" name="pg_relation" value="" type="text" placeholder="Relation">
                    <input class="span2" name="pg_tel_no" value="" type="text" placeholder="Tel. Number">
                    <input class="span1" name="pg_zip_code" value="" type="text" placeholder="Zip Code">
                </div>
              </div>
              <div style="border-top: 1px solid #DCDCDC; float: left; width: 100%; margin-top: 20px; margin-bottom: 20px;"></div>
              <div class="control-group">
                <div class="controls">
                  <input name="nops" class="span3" value="" type="text" placeholder="Name Of Previous School">
                  <select name="nops_grade_level" style="width: 120px;" required="">
                    <option hidden="" selected="" disabled="" value="">
                      Grade Level
                    </option>
                    <option>
                      Grade 7
                    </option>
                    <option>
                      Grade 8
                    </option>
                    <option>
                      Grade 9
                    </option>
                    <option>
                      Grade 10
                    </option>
                    <option>
                      Grade 11
                    </option>
                    <option>
                      Grade 12
                    </option>
                  </select>
                  <input name="nops_section" class="span2" value="" type="text" placeholder="Section">
                  <select name="nops_school_type" style="width: 120px;" required="">
                    <option selected="" hidden="" disabled="" value="">
                      School Type
                    </option>
                    <option>
                      Private
                    </option>
                    <option>
                      Public
                    </option>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <div class="controls">
                  <select id="track" name="track" style="width: 120px;" required="">
                    <option selected="" hidden="" disabled="" value="">
                      Track
                    </option>
                    <?php foreach ($tracks->result() as $value) { ?>
                      <option value="<?=$value->track;?>">
                          <?=$value->track;?>
                      </option>
                    <?php } ?>
                  </select>
                  <select id="strand" name="strand" style="width: 120px;" required="">
                    <option selected="" hidden="" disabled="" value="">
                      Strand
                    </option>
                  </select>
                  <select id="specialization" name="specialization" style="width: 150px;" required="">
                    <option selected="" hidden="" disabled="" value="">
                      Specialization
                    </option>
                  </select>
                  <select name="school_year" style="width: 120px;">
                    <option selected="" hidden="" disabled="">
                      School Year
                    </option>
                    <?php foreach ($school_year->result() as $value) { ?>
                      <option>
                        <?=$value->schoolyear;?>
                      </option>
                    <?php } ?>
                  </select>
                  <select name="grade" style="width: 120px;">
                    <option selected="" hidden="" disabled="">
                      Grade
                    </option>
                    <option>
                      Grade 11
                    </option>
                    <option>
                      Grade 12
                    </option>
                  </select>
                  <select name="semester" style="width: 120px;">
                    <option selected="" hidden="" disabled="">
                      Semester
                    </option>
                    <option>
                      1st
                    </option>
                    <option>
                      2nd
                    </option>
                  </select>
                </div>
              </div>

              <!-- Button -->
              <div class="control-group">
                <div class="controls">

                </div>
              </div>
          </div>
          <a href="<?=base_url()?>" style="border: none;float: right;font-size: 19px; padding: 15px 27px; background: #F53131;color: #ffffff;">
            Cancel
          </a>
          <button id="SubmitThis" style="margin-right: 5px; border: none;float: right;font-size: 19px; padding: 17px 27px; background: #2F99FD;color: #FFFFFF;">
            Register
          </button>
        </form>
        <!-- Form end -->
      </div>
    </div>
  </div>
</div>
</div>
<!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2013 Bootstrap Template by <a href="https://www.egrappler.com/" target="_new">Egrappler</a>. </div>
      </div>
    </div>
  </div>
</div>

<script src="<?=base_url()?>assets/egrappler/js/custom.js"></script>
<script src="<?=base_url()?>assets/egrappler/js/jquery-1.7.2.min.js"></script>
<script src="<?=base_url()?>assets/egrappler/js/excanvas.min.js"></script>
<script src="<?=base_url()?>assets/egrappler/js/chart.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>assets/egrappler/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<?=base_url()?>assets/egrappler/js/full-calendar/fullcalendar.min.js"></script>
<script src="<?=base_url()?>assets/egrappler/js/base.js"></script>
<script type="text/javascript">
    function StudID_Show() {
      var old_sel = document.getElementById('Old_Select');
      var holder = document.getElementById("in_stud");
      var stid = document.getElementById('demo');
      var data_in = '<input id="demo" class="span2" value="" type="text" name="student_id" placeholder="Student ID" style="" required="">';
      if (old_sel.checked == true)
      {
        holder.innerHTML = data_in;
        stid.style.display = "inline-block";
      }
      else
      {
        stid.outerHTML = '';
        stid.style.display = 'none';
      }

    }
</script>
<script type="text/javascript">
  $().ready(function() {
    // Get Strand
    $('#track').on('change', function() {
      var track_id = $(this).val();

      $.ajax({
        url:"<?php echo base_url()?>Registration/get_strands",
        type:"POST",
        data: {'track_id' : track_id},
        dataType: 'json',
        success: function(data)
        {
          $('#strand').html(data);
        },
        error: function()
        {
          alert('error');
        }
      });
    });

    //Get Specialization
    $('#strand').on('change', function() {
       var strand_id = $(this).val();

      $.ajax({
        url:"<?php echo base_url()?>Registration/get_specialization",
        type:"POST",
        data: {'strand_id' : strand_id},
        dataType: 'json',
        success: function(data)
        {
          $('#specialization').html(data);
        },
        error: function()
        {
          alert('No Specialization');
        }
      });
    });
  });
</script>
</body>
</html>
