<!-- Load Heading -->
<?php $this->load->view('_template/registrar/heading'); ?>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        
        <!-- Load Header -->
        <?php $this->load->view('_template/registrar/header'); ?>
        <!-- Load Side Navigation -->
        <?php $this->load->view('_template/registrar/side_nav'); ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"> <?=$Approve_Student;?> </h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item">Pre-Registered</li>
                                    <li class="breadcrumb-item active" aria-current="page"> <?=$Approve_Student;?> </li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <style type="text/css">
                                input , select
                                {
                                    border-color: #617FFF !important;
                                    color: black !important;
                                }
                            </style>
                            <form class="form-horizontal" action="<?=base_url()?>Enroll_Student" method="POST">
                                <div class="card-body" style="padding: 20px;">
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <h6>
                                                Pre - Registered Date
                                            </h6>
                                            <input value="<?=$pre_registered_date;?>" name="pre_registered_date" type="text" class="form-control" placeholder="Pre - Registered Date" readonly>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <h6>
                                                Student Number
                                            </h6>
                                            <input value="<?=$student_no;?>" name="student_no" type="text" class="form-control" placeholder="Student Number" readonly>
                                        </div>
                                        <div class="col-md-2">
                                            <h6>
                                                Student ID
                                            </h6>
                                            <input value="<?=$student_id;?>" name="student_id" type="text" class="form-control" placeholder="Student ID">
                                        </div>
                                        <div class="col-md-2">
                                            <h6>
                                                Password
                                            </h6>
                                            <input value="<?=$password;?>" name="password" type="text" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="col-md-2">
                                            <h6>
                                                LRN
                                            </h6>
                                            <input value="<?=$lrn;?>" name="lrn" type="text" class="form-control" placeholder="LRN">
                                        </div>
                                        <div class="col-md-2">
                                            <h6>
                                                Voucher
                                            </h6>
                                            <input value="<?=$voucher;?>" name="voucher" type="text" class="form-control" placeholder="Voucher">
                                        </div>
                                        <div class="col-md-2">
                                            <div class="custom-control custom-radio">
                                                <input <?=$active1;?> type="radio" class="custom-control-input" id="customControlValidation1" name="radio-stacked" value="1" required>
                                                <label class="custom-control-label" for="customControlValidation1">New</label>
                                            </div>
                                             <div class="custom-control custom-radio">
                                                <input <?=$active2;?> type="radio" class="custom-control-input" id="customControlValidation2" name="radio-stacked" value="2" required>
                                                <label class="custom-control-label" for="customControlValidation2">Old</label>
                                            </div>
                                             <div class="custom-control custom-radio">
                                                <input <?=$active3;?> type="radio" class="custom-control-input" id="customControlValidation3" name="radio-stacked" value="3" required>
                                                <label class="custom-control-label" for="customControlValidation3">Transferee</label>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div><br>
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Last Name
                                            </label>
                                            <input value="<?=$last_name;?>" name="last_name" type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                First Name
                                            </label>
                                            <input value="<?=$first_name;?>" name="first_name" type="text" class="form-control" placeholder="First Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Middle Name
                                            </label>
                                            <input value="<?=$middle_name;?>" name="middle_name" type="text" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                                Ext.
                                            </label>
                                            <input value="<?=$name_ext;?>" name="name_ext" type="text" class="form-control" placeholder="Ext.">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div><br>
                                    <br>
                                    <h6>
                                        Current Address
                                    </h6>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label>
                                                Block/Lot/Street
                                            </label>
                                            <input value="<?=$current_address;?>" name="current_address" type="text" class="form-control" placeholder="Address(block/lot/street)">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Barangay
                                            </label>
                                            <input value="<?=$current_barangay;?>" name="current_barangay" type="text" class="form-control" placeholder="Barangay">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Municipality
                                            </label>
                                            <input value="<?=$current_municipality;?>" name="current_municipality" type="text" class="form-control" placeholder="Municipality">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Province
                                            </label>
                                            <input value="<?=$current_province;?>" name="current_province" type="text" class="form-control" placeholder="Province">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div><br>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Birth Date
                                            </label>
                                            <input value="<?=$date_of_birth;?>" name="date_of_birth" type="date" class="form-control" placeholder="Date Of Birth">
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                               Age
                                            </label>
                                            <input value="<?=$student_age;?>" name="student_age" type="text" class="form-control" placeholder="Age">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <h6>
                                        Birth Place
                                    </h6>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label>
                                                Block/Lot/Street
                                            </label>
                                            <input value="<?=$bp_address;?>" name="bp_address" type="text" class="form-control" placeholder="Address(block/lot/street)">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Barangay
                                            </label>
                                            <input value="<?=$bp_barangay;?>" name="bp_barangay" type="text" class="form-control" placeholder="Barangay">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Municipality
                                            </label>
                                            <input value="<?=$bp_municipality;?>" name="bp_municipality" type="text" class="form-control" placeholder="Municipality">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Province
                                            </label>
                                            <input value="<?=$bp_province;?>" name="bp_province" type="text" class="form-control" placeholder="Province">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Religion
                                            </label>
                                            <input value="<?=$religion;?>" name="religion" type="text" class="form-control" placeholder="Religion">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Civil Status
                                            </label>
                                            <select name="civil_status" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                   <?=$civil_status;?>
                                                </option>
                                                <option>
                                                    Single
                                                </option>
                                                <option>
                                                    Married
                                                </option>
                                                <option>
                                                    Widowed
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Gender
                                            </label>
                                            <select name="student_gender" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$student_gender;?>
                                                </option>
                                                <option>
                                                    Male
                                                </option>
                                                <option>
                                                    Female
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-3">
                                            <label>
                                                Facebook Account
                                            </label>
                                            <input value="<?=$facebook_account;?>" name="facebook_account" type="text" class="form-control" placeholder="Facebook Account">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Mobile Number
                                            </label>
                                            <input value="<?=$mobile_no;?>" name="mobile_no" type="number" class="form-control" placeholder="Mobile Number">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Tel. Number
                                            </label>
                                            <input value="<?=$tel_no;?>" name="tel_no" type="text" class="form-control" placeholder="Tel. Number">
                                        </div>
                                        <div class="col-md-3">
                                            <label>
                                                Email Address
                                            </label>
                                            <input value="<?=$email_address;?>" name="email_address" type="text" class="form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Ethnicity
                                            </label>
                                            <input value="<?=$ethnicity;?>" name="ethnicity" type="text" class="form-control" placeholder="Ethnicity">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Mother Tongue
                                            </label>
                                            <input value="<?=$mother_tongue;?>" name="mother_tongue" type="text" class="form-control" placeholder="Mother Tongue">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <h6>
                                        Mother's Name
                                    </h6>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Last Name
                                            </label>
                                            <input value="<?=$m_last_name;?>" name="m_last_name" type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                First Name
                                            </label>
                                            <input value="<?=$m_first_name;?>" name="m_first_name" type="text" class="form-control" placeholder="First Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Middle Name
                                            </label>
                                            <input value="<?=$m_middle_name;?>" name="m_middle_name" type="text" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                                Ext
                                            </label>
                                            <input value="<?=$m_name_ext;?>" name="m_name_ext" type="text" class="form-control" placeholder="Ext">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Contact Number
                                            </label>
                                            <input value="<?=$m_contact;?>" name="m_contact" type="text" class="form-control" placeholder="Contact Number">
                                        </div>
                                    </div>
                                    <h6>
                                        Father's Name
                                    </h6>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Last Name
                                            </label>
                                            <input value="<?=$f_last_name;?>" name="f_last_name" type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                First Name
                                            </label>
                                            <input value="<?=$f_first_name;?>" name="f_first_name" type="text" class="form-control" placeholder="First Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Middle Name
                                            </label>
                                            <input value="<?=$f_middle_name;?>" name="f_middle_name" type="text" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                                Ext
                                            </label>
                                            <input value="<?=$f_name_ext;?>" name="f_name_ext" type="text" class="form-control" placeholder="Ext">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Contact Number
                                            </label>
                                            <input value="<?=$f_contact;?>" name="f_contact" type="text" class="form-control" placeholder="Contact Number">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <h6>
                                        In case of emergency, Please contact:
                                    </h6>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                Last Name
                                            </label>
                                            <input value="<?=$pg_last_name;?>" name="pg_last_name" type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                First Name
                                            </label>
                                            <input value="<?=$pg_first_name;?>" name="pg_first_name" type="text" class="form-control" placeholder="First Name">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                Middle Name
                                            </label>
                                            <input value="<?=$pg_middle_name;?>" name="pg_middle_name" type="text" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                                Ext
                                            </label>
                                            <input value="<?=$pg_name_ext;?>" name="pg_name_ext" type="text" class="form-control" placeholder="Ext">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-5">
                                            <label>
                                                    Address
                                            </label>
                                            <input value="<?=$pg_current_address;?>" name="pg_current_address" type="text" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-2">
                                            <label>
                                                    Relation
                                            </label>
                                            <input value="<?=$pg_relation;?>" name="pg_relation" type="text" class="form-control" placeholder="Address">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Tel. Number
                                            </label>
                                            <input value="<?=$pg_tel_no;?>" name="pg_tel_no" type="text" class="form-control" placeholder="Address">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Zip Code
                                            </label>
                                            <input value="<?=$pg_zip_code;?>" name="pg_zip_code" type="text" class="form-control" placeholder="Address">
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label>
                                                    Name of Previous School
                                            </label>
                                            <input value="<?=$nops;?>" name="nops" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Grade Level
                                            </label>
                                            <select name="nops_grade_level" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$nops_grade_level;?>
                                                </option>
                                                <option>
                                                    Grade 11
                                                </option>
                                                <option>
                                                    Grade 12
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Section
                                            </label>
                                            <input value="<?=$nops_section;?>" name="nops_section" type="text" class="form-control" placeholder="">
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    School Type
                                            </label>
                                            <select name="nops_school_type" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$nops_school_type;?>
                                                </option>
                                                <option>
                                                    Private
                                                </option>
                                                <option>
                                                    Public
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <label>
                                                    Track
                                            </label>
                                            <select name="track" id="track" class="" style="width: 100%;padding: 5px;">
                                                <option value="<?=$track;?>" selected="" hidden="">
                                                    <?=$track;?>
                                                </option>
                                                <?php foreach ($get_track->result() as $value) { ?>
                                                    <option value="<?=$value->track;?>">
                                                        <?=$value->track;?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Strand
                                            </label>
                                            <select name="strand" id="strand" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$strand;?>
                                                </option>
                                                <option value="<?=$strand;?>">
                                                    <?=$strand;?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Specialization
                                            </label>
                                            <select name="specialization" id="specialization" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$specialization;?>
                                                </option>
                                                <option>
                                                    <?=$specialization;?>
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    Grade
                                            </label>
                                            <select name="grade" id="specialization" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$grade;?>
                                                </option>
                                                <option>
                                                    Grade 11
                                                </option>
                                                <option>
                                                    Grade 12
                                                </option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <label>
                                                    School Year
                                            </label>
                                            <select name="school_year" id="specialization" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$school_year;?>
                                                </option>
                                                <?php foreach ($get_school_year->result() as $value) { ?>
                                                    <option>
                                                        <?=$value->schoolyear;?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <label>
                                                    Semester
                                            </label>
                                            <select name="semester" id="specialization" class="" style="width: 100%;padding: 5px;">
                                                <option selected="" hidden="">
                                                    <?=$semester;?>
                                                </option>
                                                <option value="1st">
                                                   1st
                                                </option>
                                                <option value="2nd">
                                                    2nd
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div style="float: left;border-bottom: 1px solid #DFDFDF;width: 100%;"></div>
                                    <br>
                                    <h6> Credentials </h6>
                                    <br>
                                    <div class="form-group row">
                                        <div class="custom-control custom-checkbox mr-sm-3">
                                            <input name="birth_certificate" type="checkbox" class="custom-control-input" id="customControlAutosizing1" value="1">
                                            <label class="custom-control-label" for="customControlAutosizing1"> Birth Certificate </label>
                                        </div>
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input name="two_by_two_picture" type="checkbox" class="custom-control-input" id="customControlAutosizing2" value="1">
                                            <label class="custom-control-label" for="customControlAutosizing2"> 2x2 Picture </label>
                                        </div>
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input name="form_138" type="checkbox" class="custom-control-input" id="customControlAutosizing3" value="1">
                                            <label class="custom-control-label" for="customControlAutosizing3"> Form 138 </label>
                                        </div>
                                        <div class="custom-control custom-checkbox mr-sm-2">
                                            <input name="good_moral" type="checkbox" class="custom-control-input" id="customControlAutosizing4" value="1">
                                            <label class="custom-control-label" for="customControlAutosizing4"> Good Moral </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-success btn-lg"> Enroll </button>
                                        <a href="<?=base_url()?>Pre_Registered_shs" type="button" class="btn btn-secondary btn-lg"> Back </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
        </div>
    </div>
</div>
<!-- Load Scripts -->
<?php $this->load->view('_template/registrar/scripts'); ?>

<script type="text/javascript">
    $().ready(function() {
        // Get Strand
        $('#track').on('change', function() {
          var track_id = $(this).val();
          
          $.ajax({
            url:"<?php echo base_url()?>Registrar/get_strands",
            type:"POST",
            data: {'track_id' : track_id},
            dataType: 'json',
            success: function(data)
            {
              $('#strand').html(data);
            },
            error: function()
            {
              alert('error');
            }
          });
        });

        //Get Specialization
        $('#strand').on('change', function() {
           var strand_id = $(this).val();
          
          $.ajax({
            url:"<?php echo base_url()?>Registrar/get_specialization",
            type:"POST",
            data: {'strand_id' : strand_id},
            dataType: 'json',
            success: function(data)
            {
              $('#specialization').html(data);
            },
            error: function()
            {
              alert('No Specialization');
            }
          });
        });
      });
</script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
</body>

</html>