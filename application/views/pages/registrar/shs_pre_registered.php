<!-- Load Heading -->
<?php $this->load->view('_template/registrar/heading'); ?>

<style>
/* The container */
.container {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 13px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* Hide the browser's default radio button */
.container input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 20px;
    width: 20px;
    background-color: #eee;
    border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.container:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.container input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the indicator (dot/circle) when checked */
.container input:checked ~ .checkmark:after {
    display: block;
}

/* Style the indicator (dot/circle) */
.container .checkmark:after {
    top: 5px;
    left: 5px;
    width: 10px;
    height: 10px;
    border-radius: 50%;
    background: white;
}
</style>
<body>
    <?php $this->session->flashdata('succes_alert'); ?>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">

        <!-- Load Header -->
        <?php $this->load->view('_template/registrar/header'); ?>
        <!-- Load Side Navigation -->
        <?php $this->load->view('_template/registrar/side_nav'); ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"> Pre-Registered Students </h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Pre-Registered</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"> Recents </h5>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-bordered table-hover table-condensed" style="width: 100%;" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Track</th>
                                                <th>Strand</th>
                                                <th>Specialization</th>
                                                <th>Grade</th>
                                                <th>Semester</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php foreach ($get_shs_student->result() as $row) { ?>
                                               <tr>
                                                    <td>
                                                       <?=$row->pre_registered_date;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->last_name.' '.$row->name_ext.', '.$row->first_name.' '.$row->middle_name;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->track;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->strand;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->specialization;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->grade;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->semester;?>
                                                   </td>
                                                   <td style="width: 87px;">
                                                       <a href="<?=base_url()?>Enrollment_form/<?=$row->student_no;?>" type="button" class="btn btn-info btn-sm">Edit</a>
                                                       <a href="<?=base_url()?><?=$row->student_no;?>" type="button" class="btn btn-danger btn-sm">Delete</a>
                                                   </td>
                                               </tr>
                                           <?php } ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Track</th>
                                                <th>Strand</th>
                                                <th>Specialization</th>
                                                <th>Grade</th>
                                                <th>Semester</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
        </div>
    </div>

<!-- Load Scripts -->
<?php $this->load->view('_template/registrar/scripts'); ?>

    <script>
        /****************************************
         *       Basic Table                   *
         ****************************************/
        $('#zero_config').DataTable();

    </script>
</body>

</html>