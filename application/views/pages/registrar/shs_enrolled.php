<!-- Load Heading -->
<?php $this->load->view('_template/registrar/heading'); ?>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">

        <!-- Load Header -->
        <?php $this->load->view('_template/registrar/header'); ?>
        <!-- Load Side Navigation -->
        <?php $this->load->view('_template/registrar/side_nav'); ?>

        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title"> Enrolled Students </h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Enrolled</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"> Recents </h5>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-bordered table-hover table-condensed" style="width: 100%;" cellspacing="0">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Track</th>
                                                <th>Strand</th>
                                                <th>Specialization</th>
                                                <th>Grade</th>
                                                <th>Semester</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($get_enrolled_student->result() as $row) { ?>
                                               <tr>
                                                    <td>
                                                       <?=$row->enrolled_date;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->last_name.' '.$row->name_ext.', '.$row->first_name.' '.$row->middle_name;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->track;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->strand;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->specialization;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->grade;?>
                                                   </td>
                                                   <td>
                                                       <?=$row->semester;?>
                                                   </td>
                                               </tr>
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
        </div>
    </div>
<!-- Load Scripts -->
<?php $this->load->view('_template/registrar/scripts'); ?>
    <script>
        $('#zero_config').DataTable();
    </script>

</body>

</html>