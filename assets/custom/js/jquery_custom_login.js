$(document).ready(function(){
    $("#submit").click(function(){
        var Username = $('#username').val();
        var Password = $('#password').val();
        // Returns successful data submission message when the entered information is stored in database.
        var dataString = 'Username1='+ Username + '&Password1='+ Password ;
            if(Username==""||Password=="")
            {
                var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>Fill all fields! </strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                $("#message").html(response).show();
            }
            else
            {
                        // AJAX Code To Submit Form.
                        $.ajax(
                        {
                            type: "POST",
                            url: "Login/Login_validation",
                            data: dataString,
                            cache: false,
                            success: function(result)
                            {
                                if (result == 'Student')
                                {
                                   window.location.replace("Student");
                                }
                                else if (result == 'Registrar') 
                                {
                                    var response = $("<div class='alert alert-dismissable alert-success'><i class='ti ti-close'></i>&nbsp; <strong>Registrar</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                    window.location.replace("Dashboard_Registrar");
                                }
                                else if (result == 'Marketing') 
                                {
                                    var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>Marketing</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                }
                                else if (result == 'Accounting') 
                                {
                                    var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>Accounting</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                }
                                else if (result == 'User dont exist') 
                                {
                                    var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>Wrong Email or Password</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                }
                                else if (result == 'user active') 
                                {
                                    var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>User is active!</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                }
                                else
                                {
                                    var response = $("<div class='alert alert-dismissable alert-danger'><i class='ti ti-close'></i>&nbsp; <strong>User dont exist</strong> <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button></div>");
                                        $("#message").html(response).show();
                                }
                            }
                        });
                    }
                return false;
    });
});